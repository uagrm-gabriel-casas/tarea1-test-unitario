import authenticationStatic.Authentication;
import authenticationStatic.CredentialsStaticService;
import authenticationStatic.PermissionStaticService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;

public class AuthenticationStaticMockdTest {
    @Test
    public void veryAuthenticationOk() {
        MockedStatic <CredentialsStaticService> credentialMocked = Mockito.mockStatic(CredentialsStaticService.class);
        MockedStatic <PermissionStaticService> permissionMocked = Mockito.mockStatic(PermissionStaticService.class);
        credentialMocked.when(()-> CredentialsStaticService.isValidCredential("admin","12345")).thenReturn(true);
        permissionMocked.when(()-> PermissionStaticService.getPermission("admin")).thenReturn("CRUD");

        Authentication authentication = new Authentication();
        // Test 1 Everything Ok
        String spectedResult = "user authenticated successfully with permission: [CRUD]";
        String currentResult = authentication.login("admin", "12345");
        Assertions.assertEquals(spectedResult, currentResult, "Error in Authentication");

        credentialMocked.close();
        permissionMocked.close();
    }

    @Test
    public void verifyAuthenticationMockFailPassword () {
        MockedStatic <CredentialsStaticService> credentialMocked = Mockito.mockStatic(CredentialsStaticService.class);
        MockedStatic <PermissionStaticService> permissionMocked = Mockito.mockStatic(PermissionStaticService.class);
        credentialMocked.when(()->CredentialsStaticService.isValidCredential("admin","1223345")).thenReturn(false);
        permissionMocked.when(()-> PermissionStaticService.getPermission("")).thenReturn("");

        Authentication authentication = new Authentication();
        // Test 1 Everything Ok
        String spectedResult = "user or password incorrect";
        String currentResult = authentication.login("admin", "1223345");
        Assertions.assertEquals(spectedResult, currentResult, "Error in Authentication");

        credentialMocked.close();
        permissionMocked.close();
    }

    @Test
    public void verifyAuthenticationMockFailUser () {
        MockedStatic <CredentialsStaticService> credentialMocked = Mockito.mockStatic(CredentialsStaticService.class);
        MockedStatic <PermissionStaticService> permissionMocked = Mockito.mockStatic(PermissionStaticService.class);
        credentialMocked.when(()->CredentialsStaticService.isValidCredential("ADMIN","12345")).thenReturn(false);
        permissionMocked.when(()-> PermissionStaticService.getPermission("")).thenReturn("");

        Authentication authentication = new Authentication();
        // Test 1 Everything Ok
        String spectedResult = "user or password incorrect";
        String currentResult = authentication.login("ADMIN", "12345");
        Assertions.assertEquals(spectedResult, currentResult, "Error in Authentication");

        credentialMocked.close();
        permissionMocked.close();
    }

    @Test
    public void verifyAuthenticationMockEmpty () {
        MockedStatic <CredentialsStaticService> credentialMocked = Mockito.mockStatic(CredentialsStaticService.class);
        MockedStatic <PermissionStaticService> permissionMocked = Mockito.mockStatic(PermissionStaticService.class);
        credentialMocked.when(()->CredentialsStaticService.isValidCredential("","")).thenReturn(false);
        permissionMocked.when(()-> PermissionStaticService.getPermission("")).thenReturn("");

        Authentication authentication = new Authentication();
        // Test 1 Everything Ok
        String spectedResult = "user or password incorrect";
        String currentResult = authentication.login("", "");
        Assertions.assertEquals(spectedResult, currentResult, "Error in Authentication");

        credentialMocked.close();
        permissionMocked.close();
    }
}

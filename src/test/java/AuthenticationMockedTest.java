import authentication.Authentication;
import authentication.CredentialsService;
import authentication.PermissionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class AuthenticationMockedTest {
    // Create two objects mock
    CredentialsService credentialsServiceMock = Mockito.mock(CredentialsService.class);
    PermissionService permissionServiceMock = Mockito.mock(PermissionService.class);

    @Test
    public void verifyAuthenticationMockOk () {
        // config the behaviour of our mock
        Mockito.when(credentialsServiceMock.isValidCredential("admin","12345")).thenReturn(true);
        Mockito.when(permissionServiceMock.getPermission("admin")).thenReturn("CRUD");

        // Set the mock to authentication
        Authentication authentication = new Authentication();
        authentication.setCredentialsPermisions(credentialsServiceMock, permissionServiceMock);

        // Test 1 Everything Ok
        String spectedResult = "user authenticated successfully with permission: [CRUD]";
        String currentResult = authentication.login("admin", "12345");
        Assertions.assertEquals(spectedResult, currentResult, "Error in Authentication");

        Mockito.verify(credentialsServiceMock).isValidCredential("admin","12345");
    }

    @Test
    public void verifyAuthenticationMockFailPassword () {
        // config the behaviour of our mock
        Mockito.when(credentialsServiceMock.isValidCredential("admin","1223345")).thenReturn(false);
        Mockito.when(permissionServiceMock.getPermission("")).thenReturn("");

        // Set the mock to authentication
        Authentication authentication = new Authentication();
        authentication.setCredentialsPermisions(credentialsServiceMock, permissionServiceMock);

        // Test 2 Password is incorrect
        String spectedResult = "user or password incorrect";
        String currentResult = authentication.login("admin", "1223345");
        Assertions.assertEquals(spectedResult, currentResult, "Error in Authentication");

        Mockito.verify(credentialsServiceMock).isValidCredential("admin","1223345");
    }

    @Test
    public void verifyAuthenticationMockFailUser () {
        // config the behaviour of our mock
        Mockito.when(credentialsServiceMock.isValidCredential("ADMIN","12345")).thenReturn(false);
        Mockito.when(permissionServiceMock.getPermission("")).thenReturn("");

        // Set the mock to authentication
        Authentication authentication = new Authentication();
        authentication.setCredentialsPermisions(credentialsServiceMock, permissionServiceMock);

        // Test 3 User is incorrect
        String spectedResult = "user or password incorrect";
        String currentResult = authentication.login("admin", "1223345");
        Assertions.assertEquals(spectedResult, currentResult, "Error in Authentication");

        Mockito.verify(credentialsServiceMock).isValidCredential("admin","1223345");
    }

    @Test
    public void verifyAuthenticationMockEmpty () {
        // config the behaviour of our mock
        Mockito.when(credentialsServiceMock.isValidCredential("","")).thenReturn(false);
        Mockito.when(permissionServiceMock.getPermission("")).thenReturn("");

        // Set the mock to authentication
        Authentication authentication = new Authentication();
        authentication.setCredentialsPermisions(credentialsServiceMock, permissionServiceMock);

        // Test 4 user and password are empty
        String spectedResult = "user or password incorrect";
        String currentResult = authentication.login("", "");
        Assertions.assertEquals(spectedResult, currentResult, "Error in Authentication");

        Mockito.verify(credentialsServiceMock).isValidCredential("","");
    }
}
